## APLICATIVO CRUD PERSONAS ANGULAR C#

###### Estructura de carpetas del repositorio
![picture](img/14.png)

# Base de datos sql server

La base de datos esta en el sql server con usuario sa y password sa:
###### Conexion al motor: Base de datos: CristhianCaycedoBasdat, Usuario: sa , Clave: sa

![picture](img/1.png)

Para configurar la base de datos cambiar el web.config linea 84 los parametros de conexion:

![picture](img/15.png)

# Backoffice en .NET

Para el back office se uso .net con el modelo MVC web api, realizando los metodos para cada una de las tres tablas a saber (personas, usuarios y perfiles):

![picture](img/2.png)

Probando el servicio de consulta SELECT personas:

![picture](img/3.png)

Probando el servicio de INSERT personas:

![picture](img/4.png)


# Front en ANGULAR

A continuacion se puede ver la carpeta de componentes y servicios:

![picture](img/5.png)

Para cambiar la URL del backoffice se hace en el servicio del front:

![picture](img/19.png)

Compilacion correcta:

![picture](img/11.png)

Ingresando una persona:

![picture](img/7.png)

![picture](img/8.png)

Listando todas las personas:

![picture](img/9.png)

Borrando una persona, se valida que se seleccione al menos un registro:

![picture](img/10.png)

Se borro correctamente la persona:

![picture](img/12.png)

Editando una persona, se valida de igual forma que al menos se seleccione un registro:

![picture](img/16.png)

Edicion exitosa de una persona:

![picture](img/17.png)

Consultamos en base de datos para confirmar 100% la actualizacion:

![picture](img/18.png)


## Validacion de campos

El sistema valida los campos y tambien en el caso del listado que se seleccione al menos un registro para poder borrarlo o editarlo:

![picture](img/13.png)