﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using cristhianCaycedoRESTFUL.Models;

namespace cristhianCaycedoRESTFUL.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class perfilesController : ApiController
    {
        private Basdat db = new Basdat();

        // GET: api/perfiles
        public IQueryable<perfile> Getperfiles()
        {
            return db.perfiles;
        }

        // GET: api/perfiles/5
        [ResponseType(typeof(perfile))]
        public async Task<IHttpActionResult> Getperfile(int id)
        {
            perfile perfile = await db.perfiles.FindAsync(id);
            if (perfile == null)
            {
                return NotFound();
            }

            return Ok(perfile);
        }

        // PUT: api/perfiles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Putperfile(int id, perfile perfile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != perfile.id)
            {
                return BadRequest();
            }

            db.Entry(perfile).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!perfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/perfiles
        [ResponseType(typeof(perfile))]
        public async Task<IHttpActionResult> Postperfile(perfile perfile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.perfiles.Add(perfile);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = perfile.id }, perfile);
        }

        // DELETE: api/perfiles/5
        [ResponseType(typeof(perfile))]
        public async Task<IHttpActionResult> Deleteperfile(int id)
        {
            perfile perfile = await db.perfiles.FindAsync(id);
            if (perfile == null)
            {
                return NotFound();
            }

            db.perfiles.Remove(perfile);
            await db.SaveChangesAsync();

            return Ok(perfile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool perfileExists(int id)
        {
            return db.perfiles.Count(e => e.id == id) > 0;
        }
    }
}