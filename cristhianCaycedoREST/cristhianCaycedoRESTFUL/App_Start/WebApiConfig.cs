﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace cristhianCaycedoRESTFUL
{
    public static class WebApiConfig
    {

        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //var cors = new EnableCorsAttribute();
            config.EnableCors();
            // Web API routes
            config.MapHttpAttributeRoutes();
            
            //config.using System.Web.Http;();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
