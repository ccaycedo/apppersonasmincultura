namespace cristhianCaycedoRESTFUL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class usuario
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [Column("usuario")]
        [StringLength(100)]
        public string usuario1 { get; set; }

        [StringLength(100)]
        public string clave { get; set; }

        public int? idPerfil { get; set; }

        public bool? activo { get; set; }
    }
}
