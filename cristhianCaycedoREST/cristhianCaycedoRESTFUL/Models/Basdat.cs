namespace cristhianCaycedoRESTFUL.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Basdat : DbContext
    {
        public Basdat()
            : base("name=Basdat")
        {
        }

        public virtual DbSet<perfile> perfiles { get; set; }
        public virtual DbSet<persona> personas { get; set; }
        public virtual DbSet<usuario> usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<perfile>()
                .Property(e => e.perfil)
                .IsUnicode(false);

            modelBuilder.Entity<persona>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<persona>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<persona>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.usuario1)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.clave)
                .IsUnicode(false);
        }
    }
}
