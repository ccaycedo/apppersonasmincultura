namespace cristhianCaycedoRESTFUL.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class persona
    {
        public int id { get; set; }

      
        public string nombre { get; set; }

        public int? cedula { get; set; }

       
        public string direccion { get; set; }

       
        public string email { get; set; }

        public bool? activo { get; set; }
    }
}
